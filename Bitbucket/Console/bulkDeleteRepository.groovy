
/*
* @Author: Jack Nolddor [Sweet Bananas] <support@sweetbananas.es>
* @Date: 26/04/2022
*
* @Description:
* Delete all repositories located under given project list
*/


import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.onresolve.scriptrunner.runner.customisers.PluginModule
import com.atlassian.bitbucket.repository.RepositoryService
import com.atlassian.bitbucket.project.ProjectService
import com.atlassian.bitbucket.util.PageRequest
import com.atlassian.bitbucket.util.PageRequestImpl

def logger = Logger.getLogger("com.nolddor")
logger.setLevel(Level.INFO)


@PluginModule
RepositoryService repositoryService

@PluginModule
ProjectService projectService



//--------------------------------------------------------------
// You must change the following variables as per your needs
//--------------------------------------------------------------
def projectKeys = ["SMPL", "LATAM"]
//--------------------------------------------------------------


def pageRequest = new PageRequestImpl(0, PageRequest.MAX_PAGE_LIMIT)

projectKeys.each{ key ->
        def project = projectService.getByKey(key)
        if(project)
        {
                def repositoriesPage = repositoryService.findByProjectKey(project.key, pageRequest)
                repositoriesPage.values.each{ repository ->
                    repositoryService.delete(repository)
                    logger.info("Repository '$repository' from Project '$key' has been deleted.")
                }
                
        }
        else
        {
                logger.warn("Project '$key' does not exist. Skipping...")
        }
}

return;
