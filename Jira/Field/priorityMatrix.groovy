/*
* @Author: Jack Nolddor [Sweet Bananas] <support@sweetbananas.es>
* @Date: 09-Oct-2019
*
* @Description:
* Basic implementation of a priority matrix using impact and urgency values
* You must use Text Field (multi-line) or Number Field Template
*
* See the links below for further details:
* https://confluence.atlassian.com/servicedeskserver/calculating-priority-automatically-939926661.html
* https://community.atlassian.com/t5/Jira-questions/Help-with-groovy-scriptrunner-calculated-field-for-non/qaq-p/1199242
*/

import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.onresolve.scriptrunner.runner.customisers.PluginModule
import com.atlassian.jira.issue.CustomFieldManager


def log = Logger.getLogger("com.nolddor")
log.setLevel(Level.DEBUG)

@PluginModule
CustomFieldManager customFieldManager


//--------------------------------------------------------------
// You must change the following variables as per your needs
//--------------------------------------------------------------
def impactFieldName = "Impact"
def urgencyFieldName = "Urgency"

def matrix = [
    [impact: 'A', urgency: 'D', priority: '1'],
    [impact: 'A', urgency: 'E', priority: '2'],
    [impact: 'A', urgency: 'F', priority: '3'],
    [impact: 'B', urgency: 'D', priority: '4'],
    [impact: 'B', urgency: 'E', priority: '5'],
    [impact: 'B', urgency: 'F', priority: '6'],
    [impact: 'C', urgency: 'D', priority: '7'],
    [impact: 'C', urgency: 'E', priority: '8'],
    [impact: 'C', urgency: 'F', priority: '9'],
]
//--------------------------------------------------------------


//Retrieve customfields from the system
def impactField = customFieldManager.getCustomFieldObjectsByName(impactFieldName).find()
def urgencyField = customFieldManager.getCustomFieldObjectsByName(urgencyFieldName).find()

// Ensure source fields really exist
if(impactField && urgencyField)
{
    // Retrieve customfield values as String
    def impact = issue.getCustomFieldValue(impactField).toString()
    def urgency = issue.getCustomFieldValue(urgencyField).toString()
    // Check for any match on our matrix
    def row = matrix.find{ row -> row.impact == impact && row.urgency == urgency}
    // Return the priority for the matching row (if any)
    return row?.priority
}
