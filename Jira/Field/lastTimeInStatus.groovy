/*
* @Author: Jack Nolddor [Sweet Bananas] <support@sweetbananas.es>
* @Date: 19-Oct-2019
*
* @Description:
* Field that returns the last time an issue was set to the given status
*
* You must use Date Time Template
* You must use Date Time Range picker Searcher
*
* See the link below for further details:
*
*/

import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.onresolve.scriptrunner.runner.customisers.PluginModule
import com.atlassian.jira.config.StatusManager
import com.atlassian.jira.issue.history.ChangeItemBean
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager

def log = Logger.getLogger("com.nolddor")
log.setLevel(Level.DEBUG)

@PluginModule
StatusManager statusManager
@PluginModule
ChangeHistoryManager changeHistoryManager


//--------------------------------------------------------------
// You must change the following variables as per your needs
//--------------------------------------------------------------
def statusName = "To Do"
//--------------------------------------------------------------


// Check if the desired status is valid
def status = statusManager.statuses.find {it.name.equalsIgnoreCase(statusName)}
if(status)
{
    // Find the date when the issue last transitioned into a required state.
    List<ChangeItemBean> changeItemsToStatus = changeHistoryManager.getChangeItemsForField(issue, "status").findAll{ status.id == it.to};
    List<ChangeItemBean> changeItemsFromStatus = changeHistoryManager.getChangeItemsForField(issue, "status").findAll{ status.id == it.from};

    if(!changeItemsToStatus.empty)
    {
        return changeItemsToStatus.last().created
    }
    else if(status.id == issue.statusId || !changeItemsFromStatus.empty)
    {
        return issue.created
    }
}
