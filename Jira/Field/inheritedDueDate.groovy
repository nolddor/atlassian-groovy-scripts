/*
* @Author: Jack Nolddor [Sweet Bananas] <support@sweetbananas.es>
* @Date: 06-Oct-2019
*
* @Description:
* Field that returns the further date from the configured linked issues types
* In addition, you can decide to not look at resolved issues
*
* You must use Date Time Template
* You must use Date Time Range picker Searcher
*
* See the link below for further details:
* https://community.atlassian.com/t5/Jira-questions/Script-to-Update-Custom-field-with-last-date/qaq-p/1196044
*/

import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.onresolve.scriptrunner.runner.customisers.PluginModule
import com.atlassian.jira.issue.link.IssueLinkManager

def log = Logger.getLogger("com.nolddor")
log.setLevel(Level.DEBUG)

@PluginModule
IssueLinkManager issueLinkManager


//--------------------------------------------------------------
// You must change the following variables as per your needs
//--------------------------------------------------------------
def sourceIssueTypes = ["Story"]
def excludeResolvedIssues = true
//--------------------------------------------------------------

// Look for all linked issues
def inwardLinkedIssues = issueLinkManager.getInwardLinks(issue.id)*.sourceObject
def outwardLinkedIssues = issueLinkManager.getOutwardLinks(issue.id)*.destinationObject
def linkedIssues = inwardLinkedIssues + outwardLinkedIssues
// Remove undesired issue types
linkedIssues = linkedIssues.findAll{it -> sourceIssueTypes.contains(it.issueType.name)}
// Remove resolved issues, if applicable
if(excludeResolvedIssues)
{
    linkedIssues = linkedIssues.findAll{!it.resolutionDate}
}
// Sort remaining linked issues by dueDate
linkedIssues.sort{it.dueDate}
// Return the further dueDate
if(linkedIssues)
{
    return linkedIssues.last().dueDate  
}

