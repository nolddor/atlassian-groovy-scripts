/*
*
* @Author: Jack Nolddor [Sweet Bananas] <support@sweetbananas.es>
* @Date: 14-Sept-2020
*
* @Description:
* Field that returns the difference between Due Date and actual Resolution Date
* Important Notes:
*  - Current date/time will be used on unresolved issues
*  - If Due Date is unset, difference won't be displayed at all
*
* You must use Duration Template
* You must use ????? Searcher
*/

import org.apache.log4j.Logger
import org.apache.log4j.Level
import java.sql.Timestamp
import java.time.LocalDateTime

def logger = Logger.getLogger("com.nolddor")
logger.setLevel(Level.DEBUG)

def now = Timestamp.valueOf(LocalDateTime.now())
def startDate = issue.dueDate ?: null as Timestamp
def endDate = issue.resolutionDate ?: now as Timestamp

if ( startDate )
{    
   return ((endDate.time - startDate.time) / 1000) as Long           
}