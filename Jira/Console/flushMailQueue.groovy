/*
* @Author: Jack Nolddor [Sweet Bananas] <support@sweetbananas.es>
* @Date: 02-Dec-2021
*
* @Description:
* Programatically try to flush outgoing mail queue.
*/


import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.mail.queue.MailQueue;
import org.apache.log4j.Logger
import org.apache.log4j.Level

def logger = Logger.getLogger("com.nolddor")
logger.setLevel(Level.DEBUG)

final MailQueue queue = ComponentAccessor.getComponent(MailQueue.class);

logger.debug("Attempting to run mail queue service")

if (queue.size() > 0 && !queue.isSending()) {
    logger.debug("Starting to send items in the mail queue...")
    queue.sendBuffer()
}
