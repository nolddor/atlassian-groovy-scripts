/*
* @Author: Jack Nolddor [Sweet Bananas] <support@sweetbananas.es>
* @Date: 09-Feb-2021
*
* @Description:
* List all customfields with no screen at all
* See https://community.atlassian.com/t5/Jira-questions/View-all-Customfields-without-Screens/qaq-p/1607106 for further information
*/

import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.onresolve.scriptrunner.runner.customisers.PluginModule
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.screen.FieldScreenManager


def logger = Logger.getLogger("com.nolddor")
logger.setLevel(Level.DEBUG)


@PluginModule
CustomFieldManager customFieldManager
@PluginModule
FieldScreenManager fieldScreenManager


def allCustomFields = customFieldManager.customFieldObjects
def allScreens = fieldScreenManager.fieldScreens

def customFieldsNoScreen = allCustomFields.findAll{ field -> !allScreens.any{ screen -> screen.containsField(field.id) } }

if(!customFieldsNoScreen.size()) {
    logger.info "All your customfields are assigned to at least one screen."
} else {
    logger.info "Found ${customFieldsNoScreen.size()} out ${allCustomFields.size()} customfields."
    customFieldsNoScreen.each{ field ->
        logger.info "Field ${field.name} (${field.id}) isn't assigned to a screen."
    }
}
