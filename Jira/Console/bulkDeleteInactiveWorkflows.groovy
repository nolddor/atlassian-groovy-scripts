/*
* @Author: Jack Nolddor [Sweet Bananas] <support@sweetbananas.es>
* @Date: 02-March-2023
*
* @Description:
* Delete inactive workflows older than the given retention period.
*/

import org.apache.log4j.Level
import org.apache.log4j.Logger
import com.onresolve.scriptrunner.runner.customisers.PluginModule
import com.atlassian.jira.workflow.WorkflowManager
import com.atlassian.jira.workflow.WorkflowSchemeManager

import java.time.LocalDateTime
import java.time.Duration

def logger = Logger.getLogger("com.nolddor")
logger.setLevel(Level.INFO)


@PluginModule
WorkflowManager workflowManager
@PluginModule
WorkflowSchemeManager workflowSchemeManager

//--------------------------------------------------------------
// You must change the following variables as per your needs
//--------------------------------------------------------------
def dryRun = false           // If enabled, it will report what changes they would have made rather than making them.
def MIN_DAYS_OLD = 30       // Inactive Workflows last modified after specified period (days) will be deleted.
//--------------------------------------------------------------

logger.info "Searching for inactive workflows schemes..."
def inactiveWorkflowSchemes = workflowSchemeManager.assignableSchemes.findAll{ !workflowSchemeManager.isActive(it) }
def inactiveWorkflowSchemesCount = inactiveWorkflowSchemes.size()
logger.info "Done! Found ${inactiveWorkflowSchemesCount} inactive workflow schemes."

inactiveWorkflowSchemes.each{ scheme -> 
    if(!dryRun) {
        workflowSchemeManager.deleteScheme(scheme.id)        
    }
    logger.info "WorkflowScheme[id=${scheme.id},name=${scheme.name}] has been deleted."
}

logger.info "Searching for inactive workflows..."
def inactiveWorkflows = workflowManager.workflows.findAll{ !it.active }
def inactiveWorkflowsCount = inactiveWorkflows.size()
logger.info "Done! Found ${inactiveWorkflowsCount} inactive workflows."

if(!inactiveWorkflowsCount) return

def now = LocalDateTime.now()
logger.info "Filtering inactive workflows older than (${MIN_DAYS_OLD}) days..."
inactiveWorkflows = inactiveWorkflows.findAll{
    def lastModified = it.getUpdatedDate()?.toLocalDateTime() ?: now
    Duration.between(lastModified, now).toDays() > MIN_DAYS_OLD
}
inactiveWorkflowsCount = inactiveWorkflows.size()
logger.info "Done! Found ${inactiveWorkflowsCount} matching inactive workflows."

if(!inactiveWorkflowsCount) return

inactiveWorkflows.each{ workflow -> 
    if(!dryRun) {
        def workflowSchemes = workflowSchemeManager.getSchemesForWorkflowIncludingDrafts(workflow)
        if(!workflowSchemes.size()) {
            workflowManager.deleteWorkflow(workflow)
        }        
    }
    logger.info "Workflow[name=${workflow.displayName}] has been deleted."
}

if(dryRun) {
    logger.info "This script has been executed on dry-run mode. Deletions have NOT been actually performed."
}
