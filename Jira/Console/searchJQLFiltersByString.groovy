/*
* @Author: Jack Nolddor [Sweet Bananas] <support@sweetbananas.es>
* @Date: 10-June-2024
*
* @Description:
* Search JQL Filters containing a given String.
*/

import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.bc.filter.SearchRequestService
import com.atlassian.jira.issue.search.SearchRequest
import com.atlassian.jira.bc.user.search.UserSearchParams
import com.atlassian.jira.bc.user.search.UserSearchService
  
// Change this constant to the string you want to search for
String TERM = 'issueFunction'
  
SearchRequestService searchRequestService = ComponentAccessor.getComponent(SearchRequestService.class)
UserSearchService userSearchService = ComponentAccessor.getComponent(UserSearchService)
def sb = new StringBuffer()
  
UserSearchParams userSearchParams = new UserSearchParams.Builder()
    .allowEmptyQuery(true)
    .includeInactive(false)
    .ignorePermissionCheck(true)
    .build()
 
 
//iterate over each user's filters
userSearchService.findUsers("", userSearchParams).each{ApplicationUser filter_owner ->
    try {
        searchRequestService.getOwnedFilters(filter_owner).each{SearchRequest filter->
            String jql = filter.getQuery().toString()?.toLowerCase()
            //for each fiilter, get JQL and check if it contains our string
            if (jql.contains(TERM?.toLowerCase())) {
                sb.append("Found: ${filter_owner.displayName}, ${filter.name} (ID=${filter.id}), ${filter.getPermissions().isPrivate() ? 'Private' : 'Shared'}, ${jql}<br/>")
            }
        }
    } catch (Exception e) {
            //if filter is private
           sb.append("Unable to get filters for ${filter_owner.displayName} due to ${e}<br/>")
    }
}
 
//output results
return sb.toString()