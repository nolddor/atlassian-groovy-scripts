/*
* @Author: Jack Nolddor [Sweet Bananas] <support@sweetbananas.es>
* @Date: 24-January-2024
*
* @Description:
* Clear resolution field within given issue.
*/

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.config.ResolutionManager
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.MutableIssue

def resolutionManager = ComponentAccessor.getComponent(ResolutionManager)
def issueManager = ComponentAccessor.issueManager
def jiraAuthenticationContext = ComponentAccessor.jiraAuthenticationContext
def issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)


//--------------------------------------------------------------
// You must change the following variables as per your needs
//--------------------------------------------------------------
def issueKey = "LATAM-51234"       // The key of the issue you want to clear the resolution.
//--------------------------------------------------------------


def issue = Issues.getByKey(issueKey) as MutableIssue
def loggedInUser = jiraAuthenticationContext.loggedInUser

issue.setResolution(null)
issueManager.updateIssue(loggedInUser, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
issueIndexingService.reIndex(issue)