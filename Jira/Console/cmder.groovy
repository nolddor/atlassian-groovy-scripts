/*
* @Author: Jack Nolddor [Sweet Bananas] <support@sweetbananas.es>
* @Date: 29-May-2020
*
* @Description:
* Run given system command as if you typed the command on a terminal
*
* Some handy commands:
*
* Check connectivity between atlassian application and other hosts
* timeout 5 telnet <host> <port>
*
* Display memory status
* free -h
*
* Display disk status
* df -h
*/

import org.apache.log4j.Level
import org.apache.log4j.Logger

def logger = Logger.getLogger("com.nolddor")
logger.setLevel(Level.DEBUG)

//--------------------------------------------------------------
// You must change the following variables as per your needs
//--------------------------------------------------------------
def command = ["free", "-h"]
def timeout = 10000 // milliseconds
//--------------------------------------------------------------

def sout = new StringBuilder(), serr = new StringBuilder()
def proc = command.execute()
proc.consumeProcessOutput(sout, serr)
proc.waitForOrKill(timeout)

if (proc.exitValue()) {
    logger.error "\n${serr}"
} else {
    logger.info "\n${sout}"
}
