/*
* @Author: Jack Nolddor [Sweet Bananas] <support@sweetbananas.es>
* @Date: 27-Nov-2020
*
* @Description:
* Upload a file to a given Confluence page using the existing Confluence primary link.
*/


import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.onresolve.scriptrunner.runner.customisers.PluginModule
import com.atlassian.applinks.api.ApplicationLinkService
import com.atlassian.applinks.api.application.confluence.ConfluenceApplicationType
import org.apache.http.entity.FileEntity
import com.atlassian.sal.api.net.Request
import com.atlassian.sal.api.net.Response
import com.atlassian.sal.api.net.ResponseException
import com.atlassian.sal.api.net.ResponseHandler
import com.atlassian.sal.api.net.RequestFilePart


def logger = Logger.getLogger("com.nolddor")
logger.setLevel(Level.DEBUG)


@PluginModule
ApplicationLinkService applicationLinkService


//--------------------------------------------------------------
// You must change the following variables as per your needs
//--------------------------------------------------------------
def pageId = "6060557"                                              // Confluence page
def filePath = "/var/atlassian/application-data/jira/dbconfig.xml"  // Attachment path
//--------------------------------------------------------------


// Ensure the existence of a working Confluence application link
def confluenceLink = applicationLinkService.getPrimaryApplicationLink(ConfluenceApplicationType.class)
assert confluenceLink : "Unable to retrieve primary Confluence application link."

// Grab the attachment from the filesystem
def file = new File(filePath)
def filePart = new RequestFilePart(file, 'file')

// REST Call
confluenceLink.createAuthenticatedRequestFactory()
    .createRequest(Request.MethodType.POST, "rest/api/content/${pageId}/child/attachment")
    .addHeader("X-Atlassian-Token", "no-check")
    .setFiles([filePart])
    .execute(new ResponseHandler<Response>() {
        @Override
        void handle(Response response) throws ResponseException {
            def responseBodyAsString = response.getResponseBodyAsString();
            if(!response.successful) {
                throw new Exception("Unable to attach file to Conflunce page (id=${pageId}).\n Http-Response: ${responseBodyAsString}");
            }
            logger.debug("File ${file.name} attached succesfully to Confluence page (id=${pageId}).\n Http-Response: ${responseBodyAsString}");
        }
    })   