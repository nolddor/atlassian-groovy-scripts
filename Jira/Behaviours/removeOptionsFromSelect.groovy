/*
* @Author: Jack Nolddor [Sweet Bananas] <support@sweetbananas.es>
* @Date: 19-Ago-2021
*
* @Description:
* Remove unwanted options from a select list field. (also works with radios & checkboxes fields)
*/

import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.component.ComponentAccessor
import groovy.transform.BaseScript
import com.onresolve.jira.groovy.user.FieldBehaviours

@BaseScript FieldBehaviours fieldBehaviours

def logger = Logger.getLogger("com.nolddor")
logger.setLevel(Level.DEBUG)

//--------------------------------------------------------------
// You must change the following variables as per your needs
//--------------------------------------------------------------
def optionsToRemove = ['Sandbox']
def fieldId = 'customfield_11503'
//--------------------------------------------------------------

def optionsManager = ComponentAccessor.optionsManager
def customFieldManager = ComponentAccessor.customFieldManager

// Getting list of all option
def customField = customFieldManager.getCustomFieldObject(fieldId)
def fieldConfig = customField.getRelevantConfig(getIssueContext())
def options = optionsManager.getOptions(fieldConfig).findAll { !it.disabled }

// Remove unwanted options & set the values back
def optionsToSet = options.findAll { !(it.value in optionsToRemove) }
getFieldById(fieldId).setFieldOptions(optionsToSet)
getFieldById(fieldId).setFormValue(optionsToSet.first()?.optionId)
