/*
* @Author: Jack Nolddor [Sweet Bananas] <support@sweetbananas.es>
* @Date: 18-Jul-2020
*
* @Description:
* Hide some issue types during creation... 
* Use behaviour 'conditions' to determine when this code is executed
* Place this code as a server-side script under Issue Type field
*/

import com.atlassian.jira.component.ComponentAccessor
import static com.atlassian.jira.issue.IssueFieldConstants.ISSUE_TYPE
import groovy.transform.BaseScript
import com.onresolve.jira.groovy.user.FieldBehaviours

@BaseScript FieldBehaviours fieldBehaviours

//--------------------------------------------------------------
// You must change the following variables as per your needs
//--------------------------------------------------------------
def excludedIssueTypeNames = ["Task", "Bug"] // These issue types will be hidden
//--------------------------------------------------------------

def allIssueTypes = ComponentAccessor.constantsManager.allIssueTypeObjects
def excludedIssueTypes = allIssueTypes.findAll { it.name in excludedIssueTypeNames }
def availableIssueTypes = allIssueTypes.minus(excludedIssueTypes)

def issueTypeField = getFieldById(ISSUE_TYPE)
issueTypeField.setFieldOptions(availableIssueTypes)
issueTypeField.setFormValue(-1)
