/*
* @Author: Jack Nolddor [Sweet Bananas] <support@sweetbananas.es>
* @Date: 01-Dec-2021
*
* @Description:
* Condition that ensures issues has been in the given status previously
*
* See the link below for further details:
* N/A
*/

import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.onresolve.scriptrunner.runner.customisers.PluginModule
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager

def logger = Logger.getLogger("com.nolddor")
logger.setLevel(Level.DEBUG)

@PluginModule
ChangeHistoryManager changeHistoryManager


//--------------------------------------------------------------
// You must change the following variables as per your needs
//--------------------------------------------------------------
def statusId = "10310"                  // Status ID you want the issue to be previously
def onlyPreviousStatus = false          // If true, will only check direct previous status instead of whole status change history
//--------------------------------------------------------------


// Look for all status changes
def previousStatuses = changeHistoryManager.getChangeItemsForField(issue, 'status').collect{it.from}
def previousStatus = previousStatuses.empty ? null : previousStatuses.last()


if(onlyPreviousStatus) {
    return (previousStatus == statusId)
} else {
    return (statusId in previousStatuses)
}
