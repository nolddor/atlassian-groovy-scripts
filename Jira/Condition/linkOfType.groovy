/*
* @Author: Jack Nolddor [Sweet Bananas] <support@sweetbananas.es>
* @Date: 28-Oct-2019
*
* @Description:
* Condition that ensures at least one issue linked using the given link description
*
* See the link below for further details:
* https://community.atlassian.com/t5/Jira-questions/Condition-for-Clone-of-issue/qaq-p/1213773#M387593
*/

import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.onresolve.scriptrunner.runner.customisers.PluginModule
import com.atlassian.jira.issue.link.IssueLinkManager

def log = Logger.getLogger("com.nolddor")
log.setLevel(Level.DEBUG)

@PluginModule
IssueLinkManager issueLinkManager


//--------------------------------------------------------------
// You must change the following variables as per your needs
//--------------------------------------------------------------
def linkDesc = "is cloned by"
//--------------------------------------------------------------


// Look for all linked issues
def anyInwardLinkedIssues = issueLinkManager.getInwardLinks(issue.id).any{issueLink -> issueLink.issueLinkType.inward.equalsIgnoreCase(linkDesc)}
def anyOutwardLinkedIssues = issueLinkManager.getOutwardLinks(issue.id).any{issueLink -> issueLink.issueLinkType.outward.equalsIgnoreCase(linkDesc)}

return (anyInwardLinkedIssues || anyOutwardLinkedIssues)
