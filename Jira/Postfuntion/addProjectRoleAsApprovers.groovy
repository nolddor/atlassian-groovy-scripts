/*
* @Author: Jack Nolddor [Sweet Bananas] <support@sweetbananas.es>
* @Date: 30-Jul-2020
*
* @Description:
* Adds all users within the given project role as approvers of current issue
*/

import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.onresolve.scriptrunner.runner.customisers.PluginModule
import com.atlassian.jira.security.roles.ProjectRoleManager
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder


def logger = Logger.getLogger("com.nolddor")
logger.setLevel(Level.DEBUG)

@PluginModule
ProjectRoleManager projectRoleManager
@PluginModule
CustomFieldManager customFieldManager

//--------------------------------------------------------------
// You must change the following variables as per your needs
//--------------------------------------------------------------
def roleName = "Developers"
def approversFieldName = "Approvers"
//--------------------------------------------------------------

def project = issue.projectObject
def rol = projectRoleManager.getProjectRole(roleName)
def actors = projectRoleManager.getProjectRoleActors(rol, project).collect{ it.users }.flatten()

//Retrieve customfields from the system
def approversField = customFieldManager.getCustomFieldObjectsByName(approversFieldName).find()

// Update the approvers customfield
approversField.updateValue(null, issue, new ModifiedValue(issue.getCustomFieldValue(approversField), actors), new DefaultIssueChangeHolder())
