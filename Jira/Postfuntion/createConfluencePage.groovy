/*
* @Author: Jack Nolddor [Sweet Bananas] <support@sweetbananas.es>
* @Date: 13-Jun-2020
*
* @Description:
* Creates a new page on a linked Confluence instance on behalf of the current user.
* This newly created page will be linked to the current issue
*/

import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.onresolve.scriptrunner.runner.customisers.PluginModule
import com.atlassian.applinks.api.ApplicationLinkService
import com.atlassian.applinks.api.ApplicationLink
import com.atlassian.applinks.api.application.confluence.ConfluenceApplicationType
import com.atlassian.jira.issue.Issue
import com.atlassian.sal.api.component.ComponentLocator
import com.atlassian.sal.api.net.Request
import com.atlassian.sal.api.net.Response
import com.atlassian.sal.api.net.ResponseException
import com.atlassian.sal.api.net.ResponseHandler
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import groovy.xml.MarkupBuilder


def logger = Logger.getLogger("com.nolddor")
logger.setLevel(Level.DEBUG)

@PluginModule
ApplicationLinkService applicationLinkService


def confluenceLink = applicationLinkService.getPrimaryApplicationLink(ConfluenceApplicationType.class)
assert confluenceLink : "Unable to retrieve a valid linked Confluence instance." // Ensure the existence of a working Confluence application link


// Use an XML builder to write page content using storage format
def writer = new StringWriter()
def xml = new MarkupBuilder(writer)


//--------------------------------------------------------------
// You must change the following variables as per your needs
//--------------------------------------------------------------

def spaceKey = "MJOBS" // Set the space key, or calculate it from the issue project or something
def parentId = 1769500 // Set the parent page (our page will be created under this page)
def pageTitle = "[${issue.key}] ${issue.summary}" // Set the page title. Should be unique in the space or page creation will fail

// Confluence page content - We must use storage format here
// Add an issue macro with the current issue
xml.'ac:structured-macro' ('ac:name': "jira") {
    'ac:parameter' ('ac:name': "key", issue.key)
    'ac:parameter' ('ac:name': "server", "Jira") // "Jira" is the Application Link Name configured on Confluence
}

// Add more paragraphs, etc
xml.p ("Some additional info here...")

//--------------------------------------------------------------


def params = [
    type: "page",
    title: pageTitle,
    space: [
        key: spaceKey
    ],
    ancestors: [
        [
            id: parentId
        ]
    ],
    body: [
        storage: [
            value: writer.toString(),
            representation: "storage"
        ]
    ]
]

confluenceLink.createAuthenticatedRequestFactory()
    .createRequest(Request.MethodType.POST, "rest/api/content")
    .addHeader("Content-Type", "application/json")
    .setRequestBody(new JsonBuilder(params).toString())
    .execute(new ResponseHandler<Response>() {
    @Override
    void handle(Response response) throws ResponseException {
        if(response.statusCode != HttpURLConnection.HTTP_OK) {
            logger.error "Unable to create linked Confluence page for issue ${issue.key}. Reason: ${response.responseBodyAsString}"
        }
    }
})
