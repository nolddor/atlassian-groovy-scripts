/*
* @Author: Jack Nolddor [Sweet Bananas] <support@sweetbananas.es>
* @Date: 21/04/2022
*
* @Description:
* Wipe all pages and blog entries from given space list
*/


import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.onresolve.scriptrunner.runner.customisers.PluginModule
import com.atlassian.confluence.spaces.SpaceManager
import com.atlassian.confluence.pages.PageManager


def logger = Logger.getLogger("com.nolddor")
logger.setLevel(Level.DEBUG)


@PluginModule
SpaceManager spaceManager

@PluginModule
PageManager pageManager


//--------------------------------------------------------------
// You must change the following variables as per your needs
//--------------------------------------------------------------
def spaceKeys = ["~avillalobos", "EMEA"]
//--------------------------------------------------------------


spaceKeys.each{ key ->
        def space = spaceManager.getSpace(key)
        if(space)
        {
                pageManager.removeAllPages(space)
                pageManager.removeAllBlogPosts(space)
                logger.info("Space '$key' pages and blog entries have been wiped.")
        }
        else
        {
                logger.warn("Space '$key' does not exist. Skipping...")
        }
}
